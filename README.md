# Wooden Enterprise D

This repository contains my templates and material around my Enterprise D project.

# Prerequisites

The versions are the ones that I used. Others may work too.

* Inkscape 1.1
* pdfposter 0.7.post.1

# Getting started

Run

```
make
```

to download blueprints

# Blog posts

* [Wooden Enterprise D (1)](https://blog.knappi.org/0022-wooden-enterprise-d-1/)

# Tools

Create A4-pdfs from one large pdf

```bash
./tools/run.sh ./tools/pdfposter.sh templates/file.pdf
```

# Trademarks and Copyright

* U.S.S. ENTERPRISE is a trademark of the Paramount Pictures Cooperation
* The blueprints that form the base of my work were made and copyrighted by Edwin Whitefire. Ed expressed his interest 
  in this project and did not oppose to me using his blueprints as basis.
* I downloaded the blueprints from on [cygnus-x1.net](http://www.cygnus-x1.net/links/lcars/ed-whitefire-enterprise-ncc-1701d.php)
  and decided not to put them into this repository for legal reasons. Instead, they are downloaded when you run "make"

# License 

All rights reserved

[see LICENSE.md](./LICENSE.md)