export function showLoadingScreen() {
  const loading = document.createElement("div");
  loading.innerText = "Loading";
  loading.classList.add("loading")

  document.body.append(loading);

  let nextLoadingDelay = 10;
  let nextLoadingTimeout: ReturnType<typeof setTimeout> | null = null;
  const updateLoader = () => {
    nextLoadingDelay *= 1.5;
    loading.innerText += " .";
    nextLoadingTimeout = setTimeout(updateLoader, nextLoadingDelay);
  };

  updateLoader()
  return {
    hideLoadingScreen() {
      loading.remove()
      if (nextLoadingTimeout != null) clearTimeout(nextLoadingTimeout);
    },
    showLoadingError(message: string) {
      loading.innerText = message
      if (nextLoadingTimeout != null) clearTimeout(nextLoadingTimeout);
    }
  };
}
