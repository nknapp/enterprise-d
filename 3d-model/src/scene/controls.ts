import { Camera, Renderer, Scene } from "three";
import { ArcballControls } from "three/examples/jsm/Addons.js";


export interface CreateControlsReturn {
    update(clockDelta: number): void;
}

export async function createControls(
  camera: Camera,
  renderer: Renderer,
  scene: Scene,
  onUpdate: () => void
): Promise<CreateControlsReturn> {
  const controls = new ArcballControls(camera, renderer.domElement, scene);
  controls.addEventListener("change", function () {
    onUpdate();
  });

  return controls;
}
