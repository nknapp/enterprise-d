import { Box3, ExtrudeGeometry, Group, Mesh, Vector3 } from "three";
import { loadSvg } from "./loadSvg";
import { decksSvg } from "../__generated__/decks";
import { createMaterial } from "./material";
import { DECK_HEIGHT } from "./constants";

export async function loadDecks(): Promise<THREE.Group> {
    const decks = new Group();
    let zShift = 0;
    for (const deck of decksSvg) {
      const deckShape = await loadDeck(deck);
      deckShape.position.z = zShift += DECK_HEIGHT;
      decks.add(deckShape);
    }
    const bbox = new Box3().setFromObject(decks);
  
    const center = bbox.getCenter(new Vector3());
    decks.position.sub(center);
  
    return decks;
  }
  
  async function loadDeck(svgFile: string): Promise<any> {
    const svg = await loadSvg(svgFile);
    const group = new Group();
    const material = await createMaterial();
    for (const path of svg.getPaths()) {
      for (const shape of path.createShapes()) {
        const extrude = new ExtrudeGeometry(shape, {
          steps: 1,
          depth: 4,
          bevelEnabled: true,
          bevelThickness: 1,
          bevelSize: 1,
          bevelOffset: 0,
          bevelSegments: 4,
        });
  
        const mesh = new Mesh(extrude, material);
        mesh.geometry.computeBoundingBox();
  
        group.add(mesh);
      }
    }
    return group;
  }