import { Shape, ShapePath } from "three";
import { SVGLoader } from "three/examples/jsm/Addons.js";

export async function loadSvg(svgFile: string): Promise<SVGModel> {
  return new SVGModel(await loadAsync(svgFile));
}

  /* @ts-ignore */
function loadAsync(svgFile: string) {
  const loader = new SVGLoader();
  return new Promise((resolve, reject) => {
    loader.load(
      svgFile,
      (data) => resolve(data),
      (xhr) => {
        console.log((xhr.loaded / xhr.total) * 100 + "% loaded");
      },
      (error) => reject(error)
    );
  });

}

export class SVGModel {
  svg: any;
  constructor(svg: any) {
    this.svg = svg
  }

  *getPaths(): Generator<SVGPath> {
    for (const path of this.svg.paths) {
        yield new SVGPath(path)
    }
  }
}

export class SVGPath {
    path: ShapePath;

    constructor (path: ShapePath) {
        this.path = path
    }

    createShapes(): Shape[] {
        return  SVGLoader.createShapes(this.path);
    }



}
