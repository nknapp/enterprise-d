import { Material, MeshStandardMaterial } from "three";

export async function createMaterial(): Promise<Material> {
    return new MeshStandardMaterial({
      color: 0xaaaaaa,
    });
  }
  
  