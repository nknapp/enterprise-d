import { AmbientLight, DirectionalLight } from "three";

export function createLights() {
  return [
    new AmbientLight("white", 1),
    directionalLight("white", 2, 0, 0, -1000),
    directionalLight("green", 4, 1000, -1000, -1000),
    directionalLight("yellow", 4, 0, 0, 1000),
  ];
}

function directionalLight(
  color: THREE.ColorRepresentation,
  intensity: number,
  x: number,
  y: number,
  z: number
): THREE.DirectionalLight {
  const light = new DirectionalLight(color, intensity);
  light.position.set(x, y, z);
  return light;
}
