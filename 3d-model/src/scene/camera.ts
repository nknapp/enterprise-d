import { PerspectiveCamera } from "three";

export function createCamera() {
  const camera = new PerspectiveCamera(
    30,
    window.innerWidth / window.innerHeight,
    0.1,
    10000
  );
  camera.position.x = 1000;
  camera.position.z = 1000;
  camera.position.y = 1000;
  return camera;
}
