import "./index.css";
import { loadEnterprise } from "./loadEnterprise";
import { showLoadingScreen } from "./showLoadingScreen";

const root = document.getElementById("root");
if (root == null) throw new Error("Root element not found");

const { hideLoadingScreen, showLoadingError } = showLoadingScreen();

attach3dModelTo(root)
  .then(() => {
    hideLoadingScreen();
  })
  .catch((error) => {
    console.error(error);
    showLoadingError(error.message);
  });

async function attach3dModelTo(el: HTMLElement) {
  const renderer = await loadEnterprise();
  el.append(renderer.domElement);
  renderer.setSize(el.clientWidth, el.clientHeight);
}
