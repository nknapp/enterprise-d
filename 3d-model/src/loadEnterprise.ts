/* @ts-ignore */
import { SVGLoader } from "three/addons/loaders/SVGLoader";
/* @ts-ignore */
import { ArcballControls } from "three/examples/jsm/controls/ArcballControls";
import { loadDecks } from "./scene/enterprise";
import { createLights } from "./scene/lights";
import { createCamera } from "./scene/camera";
import { Clock, Renderer, Scene, WebGLRenderer } from "three";
import { createControls } from "./scene/controls";



const clock = new Clock();

export async function loadEnterprise(): Promise<Renderer> {
  const scene = new Scene();
  const camera = createCamera()
  scene.add(await loadDecks());

  scene.add(...createLights());

  const renderer = new WebGLRenderer();
  const controls = await createControls(camera, renderer, scene, () => renderer.render(scene, camera));
  
  renderer.setAnimationLoop(() => {
    controls.update(clock.getDelta());
  });

  requestAnimationFrame(() => {
    renderer.render(scene, camera)
   })


  return renderer;
}


