# Wooden Enterprise 3D-Model

## Getting started

Run 

```
npm i
npm run dev
```

to start dev-server

## Updating model

The model is derive from the svg file `deck-master/all-deck-outlines.svg`.
Run 

```
npm run extract-decks
```

to extract the (numbered) deck layers from the master file into `src/__generated__/decks`.
Remember to check in these files. They are not automatically recreated with each build.

## Running tests

Sorry, there are no tests. Everything in here is explorational. I have never used "three.js" before 
and I honestly don't see a good way to test most of this code automatically.

## Building

```
npm run build
```
