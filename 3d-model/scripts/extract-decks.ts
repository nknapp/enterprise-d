import { program } from "commander";
import { readSvg } from "./extract-decks/readSvg";
import { writeDeck } from "./extract-decks/writeDeck";
import path from "node:path";
import fs from "node:fs/promises";
import { writeIndex } from "./extract-decks/writeIndex";

program
  .argument("<svgFile>", "Path to the deck-by-deck svg file")
  .option("-d, --deck <name>", "Extract decks", "all")
  .action(async (svgFile) => {
    const options = program.opts();
    const outputDir = "src/__generated__/decks";
    await fs.mkdir(outputDir, { recursive: true });
    const deckByDeck = await readSvg(svgFile);
    console.log("Converting layers");
    for (const deck of deckByDeck.decks) {
      if (options.deck === "all" || options.deck === deck.name) {
        const filename = path.join(outputDir, deck.name + ".svg");
        await writeDeck(deckByDeck.filename, deck.id, filename);
      }
    }
    console.log("Writing index.ts");
    await writeIndex(outputDir, deckByDeck);
  });

program.parse();
