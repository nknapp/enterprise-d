import cp from "node:child_process"

export function writeDeck(inputFile: string, layerId: string, outputFile: string) {
    const cmd = "inkscape"
    const args = [
        `--export-filename=${outputFile}`,
        "--export-type=svg",
        "--export-plain-svg",
        "--export-area-page",
        `--export-id=${layerId}`,
        `--export-id-only`,
        inputFile
      ]
      console.log("Running", `${cmd} ${args.join(" ")}`)
    console.log(cp.execFileSync(cmd, args, {encoding: "utf-8"}))
}