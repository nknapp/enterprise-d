import { Cheerio, CheerioAPI, load } from "cheerio";
import fs from "node:fs/promises";

export interface DeckByDeckData {
  filename: string;
  decks: Array<{
    name: string;
    id: string;
  }>;
}

export async function readSvg(pathToFile: string): Promise<DeckByDeckData> {
  const svgString = await fs.readFile(pathToFile, "utf-8");
  const $ = load(svgString);

  return {
    filename: pathToFile,
    decks: getDecks($),
  };
}

function getDecks($: CheerioAPI): DeckByDeckData["decks"] {
  return $('g[inkscape\\:groupmode="layer"]')
    .toArray()
    .reverse()
    .filter((element) => {
      return element.attribs["inkscape:label"].match(/^\d+(upper|lower)?$/);
    })
    .map((element) => {
      return {
        name: element.attribs["inkscape:label"],
        id: element.attribs.id,
      };
    });
}
