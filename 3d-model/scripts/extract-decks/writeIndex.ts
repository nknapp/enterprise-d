import { fstat } from "fs";
import { DeckByDeckData } from "./readSvg";
import fs from "node:fs/promises";
import path from "node:path";

export async function writeIndex(
  outputDir: string,
  decks: DeckByDeckData
): Promise<void> {
  const imports = decks.decks.map((deck) => {
    return `import ${varname(deck)} from '${filename(deck)}'`;
  });

  const entries = decks.decks.map(varname)

  await fs.writeFile(
    path.join(outputDir, "index.ts"),
    `
${imports.join("\n")}
    
export const decksSvg = [
    ${entries.join(", ")}
]
    `
  );
}

function varname(deck: DeckByDeckData["decks"][number]): string {
  return `deck${deck.name}`;
}

function filename(deck: DeckByDeckData["decks"][number]): string {
    return "./" + deck.name + ".svg"
}
