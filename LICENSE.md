All rights reserved (c) Nils Knappmeier, 2024

I usually publish my stuff as open-source. In this case I am not sure if I can do so, because the underlying blueprints are copyrighted to Ed Whitefire.

* If you want to build your own Enterprise D, you can use everything here to do this.
* If you want to sell a building kit for money, please do not do so without my consent (and that of Ed I suppose)

