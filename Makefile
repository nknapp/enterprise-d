BLUEPRINT_SHEETS = deck-master/blueprint-images/whitefire-enterprise-ncc-1701d-sheet-1.jpg \
	deck-master/blueprint-images/whitefire-enterprise-ncc-1701d-sheet-2.jpg \
	deck-master/blueprint-images/whitefire-enterprise-ncc-1701d-sheet-3.jpg \
	deck-master/blueprint-images/whitefire-enterprise-ncc-1701d-sheet-4.jpg \
	deck-master/blueprint-images/whitefire-enterprise-ncc-1701d-sheet-5.jpg \
	deck-master/blueprint-images/whitefire-enterprise-ncc-1701d-sheet-6.jpg \
	deck-master/blueprint-images/whitefire-enterprise-ncc-1701d-sheet-7.jpg \
	deck-master/blueprint-images/whitefire-enterprise-ncc-1701d-sheet-8.jpg \
	deck-master/blueprint-images/whitefire-enterprise-ncc-1701d-sheet-9.jpg \
	deck-master/blueprint-images/whitefire-enterprise-ncc-1701d-sheet-10.jpg \
	deck-master/blueprint-images/whitefire-enterprise-ncc-1701d-sheet-11.jpg \
	deck-master/blueprint-images/whitefire-enterprise-ncc-1701d-sheet-12.jpg \
	deck-master/blueprint-images/whitefire-enterprise-ncc-1701d-sheet-13.jpg

SVG_PAGE_OBJECTS = page-engineering-1 \
				page-engineering-2 \
				page-nacelle-pylon-bending-template \
				page-deck-35 \
				page-deck-36

PAGES = $(patsubst page-%,pages/%.pdf,$(SVG_PAGE_OBJECTS))

POSTERS = $(patsubst pages/%.pdf,posters/%.poster.pdf,$(PAGES))

FLIPPED_POSTERS = $(patsubst %.poster.pdf,%.poster-flipped.pdf,$(POSTERS))

CHECKSUMMED_FILES = $(BLUEPRINT_SHEETS)

.PHONY: all
all: verify-checksums flipped-poster-pages

.PHONY: update-checksums
update-checksums: $(CHECKSUMMED_FILES)
	sha512sum deck-master/blueprint-images/whitefire-enterprise-ncc-1701d-sheet-*.jpg >sha512sum.txt

deck-master/blueprint-images/whitefire-enterprise-ncc-1701d-sheet-%.jpg:
	curl --silent -o "$@" --fail-early https://www.cygnus-x1.net/links/lcars/blueprints/whitefire-enterprise-ncc-1701d/whitefire-enterprise-ncc-1701d-sheet-"$*".jpg

.PHONY: verify-checksums
verify-checksums: $(CHECKSUMMED_FILES)
	sha512sum -c <sha512sum.txt


.PHONY: delete-blueprints
delete-blueprints:
	rm $(BLUEPRINT_SHEETS)
			 
.PHONY: flipped-poster-pages
flipped-poster-pages: $(FLIPPED_POSTERS) $(POSTERS) $(PAGES)

posters/%.poster-flipped.pdf: posters/%.poster.pdf
	pdfjam --outfile "$@" --suffix flipped --reflect true "$<"

posters/%.poster.pdf: pages/%.pdf
	./tools/run.sh ./tools/pdfposter.sh "$<" "$@"

pages/%.pdf: deck-master/all-deck-outlines.svg
	inkscape "$<" --export-filename=- --export-id="page-$(*F)" --export-type=svg | \
		inkscape --pipe --export-filename="$@"

