#!/bin/bash -x

set -e

if [[ $# -ne 2 ]] ; then
    echo "Usage: $0 <input-file> <output-file>"
    echo 
    echo Will create posterized version of the pdf, with margins and crop-marks
    exit 1
fi

SOURCE_FILE="$1"
TARGET_FILE="$2"
DIR_NAME="$( dirname $TARGET_FILE )"
LATEX_JOB_NAME="$( basename $TARGET_FILE .pdf)"

TMP_FILE=$( mktemp --suffix .pdf )

pdfposter -m160x247mm -s1 "$SOURCE_FILE" "$TMP_FILE"

# https://leolca.blogspot.com/2010/06/pdfposter.html

pdflatex --output-directory ${DIR_NAME} --jobname "$LATEX_JOB_NAME" -- <<TEXDOC
\documentclass{article}
% Support for PDF inclusion 
\usepackage[final]{pdfpages}
% Support for PDF scaling
\usepackage{graphicx}
\usepackage[dvips=false,pdftex=false,vtex=false]{geometry}
\geometry{
   paperwidth=160mm,
   paperheight=247mm,
   margin=2.5mm,
   top=2.5mm,
   bottom=2.5mm,
   left=2.5mm,
   right=2.5mm,
   nohead
}
\usepackage[cam,a4,center,dvips]{crop}
\begin{document}
% Globals: include all pages, don't auto scale
\includepdf[pages=-,pagecommand={\thispagestyle{plain}}]{${TMP_FILE}}
\end{document}
TEXDOC

rm -f "$DIR_NAME/$LATEX_JOB_NAME"{.aux,.log} texput.log

